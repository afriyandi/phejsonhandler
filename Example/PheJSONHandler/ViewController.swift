//
//  ViewController.swift
//  PheJSONHandler
//
//  Created by Afriyandi Setiawan on 11/24/2016.
//  Copyright (c) 2016 Afriyandi Setiawan. All rights reserved.
//

import UIKit
import PheJSONHandler

var _param:PheJSONHandler?

var param:PheJSONHandler? {
    get {
        if _param == nil {
            _param = PheJSONHandler()
        }
        return _param
    }
    set {
        _param = newValue
    }
}

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let _name: Array<String> = [
            "user",
            "password",
            "type",
            ]
        let _value: Array<String> = [
            "warsono07@gmail.com",
            "password",
            "c",
            ]
        let b:DataBuilder = DataBuilder()
        let a = b.build(formName: _name, formValue: _value)
        param?.urlNya = "user/login"
        param?.connectionParam = a
        param?.routinePost { (finish, message) in
            print(message)
            param = nil
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        let _name: Array<String> = [
            "state",
            "parameter",
            ]
        let _value: Array<String> = [
            "u".capitalized,
            "warsono07",
            ]
        super.viewDidAppear(animated)
        let b:DataBuilder = DataBuilder()
        let a = b.build(formName: _name, formValue: _value)
        param?.urlNya = "user/check"
        param?.connectionParam = a
        param?.routinePost { (finish, message) in
            print(message)
            param = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func refresh(_ sender: UIButton) {
        let _name: Array<String> = [
            "user",
            "password",
            "type",
            ]
        let _value: Array<String> = [
            "warsono07@gmail.com",
            "password",
            "c",
            ]
        let b:DataBuilder = DataBuilder()
        let a = b.build(formName: _name, formValue: _value)
        param?.urlNya = "user/login"
        param?.connectionParam = a
        param?.routinePost { (finish, message) in
            print(message)
            param = nil
        }
    }
}

