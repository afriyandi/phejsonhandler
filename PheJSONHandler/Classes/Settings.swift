//
//  Settings.swift
//  Pods
//
//  Created by Afriyandi Setiawan on 9/30/16.
//
//

import Foundation

//MARK: App setting
struct Settings {
    static func URL() -> String {
        return "http://technician-oncall.com/toc/api/"
    }
    
    static func AppKey() -> String {
        return "3f30a837bb81372eced861509d55ca5d"
    }
    
    static func UserAgent() -> String {
        return "TOC/\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String) (TOC for \(UIDevice.current.model); iOS version \(UIDevice.current.systemVersion)) ~ phe"
    }
}

public struct PheJSONHandlerSettings {
    
    private var base:String?
    public var baseURL:String {
        set {
            base = newValue
        }
        get {
            guard base != nil else {
                return "http://technician-oncall.com/api/"
            }
            return base!
        }
    }
    
    private var appKey:String?
    public var AppKey:String {
        set {
            appKey = newValue
        }
        get {
            guard appKey != nil else {
                return "3f30a837bb81372eced861509d55ca5d"
            }
            return appKey!
        }
    }
    
    public init() {}
    
    public init(base: String!, appKey: String!) {
        self.baseURL = base
        self.AppKey = appKey
    }
    
    static var UserAgent:String {
        return "TOC/\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String) (TOC for \(UIDevice.current.model); iOS version \(UIDevice.current.systemVersion)) ~ phe"
    }
}
