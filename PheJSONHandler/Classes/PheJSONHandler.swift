import Foundation
import AFNetworking
#if useCrypto
    import CryptoSwift
#endif

open class PheJSONHandler {
    
    private var param:ConnectionParams?
    public var connectionParam:ConnectionParams? {
        set {
            param = newValue
        }
        get {
            return param
        }
    }
    
    public weak var OpMgr:AFHTTPSessionManager?
    private weak var indicator:AFNetworkActivityIndicatorManager?
    
    private var url:String?
    public var urlNya:String? {
        set {
            url = newValue
        }
        get {
            return url
        }
    }
    
    public typealias PassingBlock = (Bool, Any) -> Void
    
    public typealias progress = (Progress) -> Void
    
    public required init(url: String!, param: ConnectionParams!){
        self.connectionParam = param
        self.urlNya = url
        self.connectionManagerSet(connectionSettings: PheJSONHandlerSettings())
    }
    
    public init() {
        self.connectionManagerSet(connectionSettings: PheJSONHandlerSettings())
    }
    
    public init(baseUrl: String!, appKey: String? = nil) {
        var setting:PheJSONHandlerSettings = PheJSONHandlerSettings()
        setting.baseURL = baseUrl
        if appKey != nil {
            setting.AppKey = appKey!
        }
        self.connectionManagerSet(connectionSettings: setting)
    }
    
    public init(param: ConnectionParams!, connectionSetting: PheJSONHandlerSettings!) {
        self.connectionParam = param
        self.connectionManagerSet(connectionSettings: connectionSetting)
    }
    
    func seriliazing(appKey: String?) -> (request: AFHTTPRequestSerializer, response: AFHTTPResponseSerializer) {
        let requestSerial = AFHTTPRequestSerializer()
        let responsSerial = AFHTTPResponseSerializer()
        requestSerial.setValue(PheJSONHandlerSettings.UserAgent, forHTTPHeaderField: "User-Agent")
        requestSerial.setValue(appKey, forHTTPHeaderField: "X-App-Key")
        requestSerial.timeoutInterval = 30
        requestSerial.allowsCellularAccess = true
        let acceptAble:Array<String> = ["application/json", "text/html", "text/plain", "image/jpeg", "image/png"]
        responsSerial.acceptableContentTypes = Set(acceptAble)
        return (requestSerial, responsSerial)
    }
    
    func connectionManagerSet(connectionSettings: PheJSONHandlerSettings) {
        indicator = AFNetworkActivityIndicatorManager()
        indicator?.isEnabled = true
        let seriliaze = self.seriliazing(appKey: connectionSettings.AppKey)
        OpMgr = AFHTTPSessionManager(baseURL: URL(string: connectionSettings.baseURL) as URL?)
        OpMgr?.requestSerializer.cachePolicy = .returnCacheDataElseLoad
        OpMgr?.requestSerializer = seriliaze.request
        OpMgr?.responseSerializer = seriliaze.response
        let securityPolicy = AFSecurityPolicy(pinningMode: .none)
        securityPolicy.validatesDomainName = true
        securityPolicy.allowInvalidCertificates = false
        OpMgr?.securityPolicy = securityPolicy
    }

    deinit {
        OpMgr?.invalidateSessionCancelingTasks(true)
        indicator = nil
    }
}

extension PheJSONHandler {
    
    public func routineUpload(file: Data!, rsp:@escaping PassingBlock, prog:@escaping progress) -> Void {
        
        guard let urlNya = self.urlNya, let connectionParam =  self.connectionParam else {
            rsp(false, "Parameter not satisfied")
            return
        }
        
        OpMgr?.post(urlNya,
                   parameters: getParam(),
                   constructingBodyWith: { (formData: AFMultipartFormData) in
                    formData.appendPart(withFileData: file, name: connectionParam.formName!, fileName: connectionParam.fileName!, mimeType: connectionParam.mimeType!)
        },
                   progress: {(progValue) in
                    prog(progValue)
        },
                   success: { (op, response) in
                    guard let themessage:AnyObject  = response as AnyObject? else {
                        rsp(false, "NO Object" as AnyObject)
                        return
                    }
                    rsp(true, self.parserResult(message: themessage as! Data))
                    
        }) { (op, error) in
            rsp(false, error.localizedDescription as AnyObject)
        }
        
    }
    
    public func routineGet(rsp:@escaping PassingBlock) -> Void{

        guard let urlNya = self.urlNya, let _ =  self.connectionParam else {
            rsp(false, "Parameter not satisfied")
            return
        }
        
        OpMgr?.get(urlNya,
                  parameters: getParam(),
                  progress: nil,
                  success: { (op, response) in
                    guard let themessage:AnyObject  = response as AnyObject? else {
                        rsp(false, "NO Object" as AnyObject)
                        return
                    }
                    rsp(true, self.parserResult(message: themessage as! Data))
                    
        }) { (op, error) in
            rsp(false, error.localizedDescription as AnyObject)
        }
    }
    
    public func routinePut(rsp:@escaping PassingBlock) -> Void{
        
        guard let urlNya = self.urlNya, let _ =  self.connectionParam else {
            rsp(false, "Parameter not satisfied")
            return
        }
        
        OpMgr?.put(urlNya,
                  parameters: getParam(),
                  success: { (op, response) in
                    guard let themessage:AnyObject  = response as AnyObject? else {
                        rsp(false, "NO Object" as AnyObject)
                        return
                    }
                    rsp(true, self.parserResult(message: themessage as! Data))
        }) { (op, error) in
            rsp(false, error.localizedDescription as AnyObject)
        }
    }
    
    public func routineDelete(rsp:@escaping PassingBlock) -> Void{
        
        guard let urlNya = self.urlNya, let _ =  self.connectionParam else {
            rsp(false, "Parameter not satisfied")
            return
        }
        
        OpMgr?.delete(urlNya,
                     parameters: getParam(),
                     success: { (op, response) in
                        guard let themessage:AnyObject  = response as AnyObject? else {
                            rsp(false, "NO Object" as AnyObject)
                            return
                        }
                        rsp(true, self.parserResult(message: themessage as! Data))
        }) { (op, error) in
            rsp(false, error.localizedDescription as AnyObject)
        }
    }
    
    public func routinePost(rsp:@escaping PassingBlock)-> Void{
        
        guard let urlNya = self.urlNya, let _ =  self.connectionParam else {
            rsp(false, "Parameter not satisfied")
            return
        }
        
        OpMgr?.post(urlNya,
                   parameters: getParam(),
                   progress: nil,
                   success: { (op, response) in
                    guard let themessage:AnyObject  = response as AnyObject? else {
                        rsp(false, "NO Object" as AnyObject)
                        return
                    }
                    rsp(true, self.parserResult(message: themessage as! Data))
        }) { (op, error) in
            rsp(false, error.localizedDescription as AnyObject)
        }
    }
    
    public func cancelRequest() {
        OpMgr?.operationQueue.cancelAllOperations()
    }
    
    func getParam() -> NSDictionary? {
        guard let formData = self.connectionParam!.formData else {
            return nil
        }
        guard let name = formData["name"] as? Array<NSCopying> else {
            return nil
        }
        guard let value =  formData["value"] as? Array<AnyObject> else {
            return nil
        }
        return NSDictionary(objects: value, forKeys: name)
    }
}

extension PheJSONHandler {
    
    func parserResult(message: Data) -> Any {
        var responseStr:String = String()
        
        //return as what it is
        guard let rsp:String = NSString(data: message as Data, encoding: String.Encoding.utf8.rawValue) as String? else {
            return message as AnyObject
        }
        
        responseStr = rsp
        var JSON:Any?
        do {
            JSON = try JSONSerialization.jsonObject(with: message, options:[])
            guard let _ :Array<String> = JSON as? Array<String> else {
                return JSON as! Dictionary<String, AnyObject>
            }
            return JSON as! Array<String>
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        catch {
            print("hell if i know")
        }
        return responseStr as AnyObject
    }
}
