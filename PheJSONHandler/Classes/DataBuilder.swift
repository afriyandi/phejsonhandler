//
//  DataBuilder.swift
//  Pods
//
//  Created by Afriyandi Setiawan on 9/30/16.
//
//

import Foundation

public class DataBuilder: NSObject {
    
    public func build(formName: Array<String>, formValue: Array<Any>, formFileName: Array<String>? = nil, formFileValue: Array<Any>? = nil) -> ConnectionParams? {
        var appendThis:Dictionary<String, Dictionary<String, Any>> = Dictionary(dictionaryLiteral: (ConnectionKeys.multiform.rawValue, Dictionary(dictionaryLiteral: (ConnectionKeys.formdata.rawValue, Dictionary(dictionaryLiteral: (ConnectionKeys.name.rawValue, formName), (ConnectionKeys.value.rawValue, formValue))))))
        if let formName = formFileName, let formValue = formFileValue {
            var dict:Dictionary<String, Any> = Dictionary<String, Any>()
            for (index, element) in formName.enumerated()
            {
                dict[element] = formValue[index]
            }
            appendThis[ConnectionKeys.multiform.rawValue]?[ConnectionKeys.formfile.rawValue] = dict
        }
        var dict = ConnectionDicts()
        dict[.multiform] = appendThis
        var param = ConnectionParams(connectionDicts: dict)
        do {
            param.json = try JSONSerialization.data(withJSONObject: appendThis, options: JSONSerialization.WritingOptions.prettyPrinted)
            return param
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        return nil;
    }
}

typealias ConnectionDicts = [ConnectionKeys:Dictionary<String, Any>]

public enum ConnectionKeys:String {
    case formdata = "formdata"
    case formfile = "formfile"
    case multiform = "multiform"
    case dict = "dict"
    case name = "name"
    case value = "value"
    case json = "json"
    case filename = "filename"
    case mimeType = "mimeType"
}

protocol ConnectionValues {
    var json: Data? {get}
    var fileName: String? {get}
    var mimeType: String? {get}
    var formName: String? {get}
    var multiForm: Dictionary<String, Any> {get}
    var formData: Dictionary<String, Any>? {get}
}

public struct ConnectionParams : ConnectionValues {
    public var json: Data?
    public var fileName: String?
    public var mimeType: String?
    public var formName: String?
    public var multiForm: Dictionary<String, Any>
    public var formData: Dictionary<String, Any>?
    init(connectionDicts: ConnectionDicts) {
        multiForm = connectionDicts[.multiform]!
        let _mf:Dictionary<String, Any> = (multiForm[ConnectionKeys.multiform.rawValue] as? Dictionary<String, Any>)!
        guard let _formData = _mf[ConnectionKeys.formdata.rawValue] as? Dictionary<String, Any> else {
            return
        }
        formData = _formData
        guard let formfile = _mf[ConnectionKeys.formfile.rawValue] as? Dictionary<String, Any> else {
            return
        }
        fileName = formfile[ConnectionKeys.filename.rawValue] as? String
        mimeType = formfile[ConnectionKeys.mimeType.rawValue] as? String
        formName = formfile[ConnectionKeys.name.rawValue] as? String
    }
}
