#
# Be sure to run `pod lib lint PheJSONHandler.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PheJSONHandler'
  s.version          = '0.1.1'
  s.summary          = 'A short description of PheJSONHandler.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/<GITHUB_USERNAME>/PheJSONHandler'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Afriyandi Setiawan' => 'afreeyandi@gmail.com' }
  s.source           = { :git => 'https://github.com/<GITHUB_USERNAME>/PheJSONHandler.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'PheJSONHandler/Classes/**/*'

  s.subspec 'PheJSONHandler' do |cs|
    cs.dependency 'AFNetworking'
    # cs.dependency 'CryptoSwift', '~> 0.5.2'
  end

  # s.resource_bundles = {
  #   'PheJSONHandler' => ['PheJSONHandler/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking'
  # s.dependency 'AFNetworking', '~> 2.3'
end
