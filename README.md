# PheJSONHandler

[![CI Status](http://img.shields.io/travis/Afriyandi Setiawan/PheJSONHandler.svg?style=flat)](https://travis-ci.org/Afriyandi Setiawan/PheJSONHandler)
[![Version](https://img.shields.io/cocoapods/v/PheJSONHandler.svg?style=flat)](http://cocoapods.org/pods/PheJSONHandler)
[![License](https://img.shields.io/cocoapods/l/PheJSONHandler.svg?style=flat)](http://cocoapods.org/pods/PheJSONHandler)
[![Platform](https://img.shields.io/cocoapods/p/PheJSONHandler.svg?style=flat)](http://cocoapods.org/pods/PheJSONHandler)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PheJSONHandler is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PheJSONHandler"
```

## Author

Afriyandi Setiawan, afreeyandi@gmail.com

## License

PheJSONHandler is available under the MIT license. See the LICENSE file for more info.
